interface Demo{

	void fun();
	void gun();

}

class DChild implements Demo{

    public void fun(){

		System.out.println("In Fun");
	}

    public void gun(){

		System.out.println("In gun");
	}
}

class Client{

	public static void main(String args[]){

	DChild obj=new DChild();
	obj.fun();
	obj.gun();

	}

}

