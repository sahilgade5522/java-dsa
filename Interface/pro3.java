interface Demo1{

        void fun();
	void gun();

}

abstract class DemoChild implements Demo1{

        public void fun(){

                System.out.println("In fun Child");
        }

}

class Child extends DemoChild{

	public void gun(){

		   System.out.println("In gun Child");

	}
}


class Client{

        public static void main(String args[]){

            Demo1 obj=new Child();

                obj.fun();
		obj.gun();
        }
}

