import java.util.Scanner;

class pro1{
        
        public static void factorial(int start,int end){
    
        for(int i=start;i<=end;i++){

            System.out.print("Factorial of "+i+" is: ");
            int fact=1;
         for(int j=1;j<=i;j++){
            
            fact=fact*j;
         }
         System.out.print(fact);
         System.out.println();
        }
    }
        public static void main(String[] args) {
            
            Scanner sc=new Scanner(System.in);
            System.out.println("Enter start");
            int start=sc.nextInt();
    
            System.out.println("Enter end");
            int end=sc.nextInt();
    
            factorial(start,end);
        }
    }
