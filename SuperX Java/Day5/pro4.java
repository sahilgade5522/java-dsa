import java.util.Scanner;

public class pro4 {
    
    public static void PrintPattern4(int n){

        int count=0;
        int no=1;
        for(int i=1;i<=n;i++){
           count++;
            for(int j=1;j<=n;j++){
                if(count<=2){
                System.out.print(no+" ");
                no=no+2;
                }
                if(count>2){
                    System.out.print(no+" ");
                    no=no+2;
                }
            }
            if(i%2==1){
                no=no-7;
            }
            else{
                no--;
            }

            System.out.println();
        }
    }
    public static void main(String[] args) {
        
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter n");
        int n=sc.nextInt();

        PrintPattern4(n);

    }
}
