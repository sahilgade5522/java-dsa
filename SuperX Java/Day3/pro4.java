import java.util.Scanner;

public class pro4 {

    public static void ReverseNo(int start,int end){
   
        for(int i=start;i<=end;i++){
        int no=i;
        int rev=0;
        while(no!=0){

            int rem=no%10;

             rev=rev*10+rem;
             no=no/10;
        }
        System.out.println(rev);
    }
    }
    public static void main(String[] args) {
        
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter start");
        int start=sc.nextInt();

        System.out.println("Enter end");
        int end=sc.nextInt();

       ReverseNo(start,end);
    }
}

