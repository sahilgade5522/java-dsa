import java.util.Scanner;

public class pro3 {

    public static void palindromeNo(int n){
   
        int no=n;
        int rev=0;
        while(no!=0){

            int rem=no%10;

             rev=rev*10+rem;
             no=no/10;
        }
        if(rev==n){
            System.out.println(n+" is Palindrome");
        }
        else{
            System.out.println(n+" is not Palindrome");
        }

    }
    public static void main(String[] args) {
        
         Scanner sc=new Scanner(System.in);
        System.out.println("Enter n");
        int n=sc.nextInt();

        palindromeNo(n);
    }
}

