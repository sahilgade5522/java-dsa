import java.util.Scanner;

public class pro5 {
    
    public static void checkStr(String str){
        char ch[]=str.toCharArray();
        int flag=0;
        for(int i=0;i<ch.length;i++){

            int cht=ch[i];

            if(cht>=65 && cht<=90 || cht>=97 && cht<=122){

                continue;
            }
            else{
                flag=1;
                break;
            }
        }
        if (flag==0){
            System.out.println("String contains Characters only");
        }
        else{
            System.out.println("String contains other letters also");
        }
    }
    public static void main(String[] args) { 
       
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter string");
        String str=sc.nextLine();

         checkStr(str);
}
}