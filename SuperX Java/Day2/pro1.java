import java.util.Scanner;
public class pro1 {

    public static void PrintPattern1(int n){
        char ch='A';
        for(int i=1;i<=n;i++){

            char c=ch;
            for(int j=1;j<=n;j++){

                System.out.print(c+" ");
                c++;
            }
            System.out.println();
            ch++;
        }
    }
    public static void main(String[] args) {
         Scanner sc=new Scanner(System.in);
        System.out.println("Enter n");
        int n=sc.nextInt();

        PrintPattern1(n);
    }
}
