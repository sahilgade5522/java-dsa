import java.util.Scanner;

class pro1{
    
        public static void PrintPattern1(int n,int m){
            char ch='A';
            int no=1;
            for(int i=1;i<=n;i++){
                 char ch1=ch;
                for(int j=1;j<=m;j++){
    
                    if(i%2==1){
    
                          System.out.print(ch1+"  ");
                         ch1++;
                    }
                    else{
                        System.out.print(no+"  ");
                        no=no+2;
                    }
                }
                System.out.println();
            }
        }
        public static void main(String[] args) {
             Scanner sc=new Scanner(System.in);
            System.out.println("Enter rows");
            int n=sc.nextInt();

            System.out.println("Enter columns");
            int m=sc.nextInt();
    
            PrintPattern1(n,m);
        }
    }
