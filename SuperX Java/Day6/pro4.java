import java.util.Scanner;
public class pro4 {

    public static void PrintStrongNo(int start,int end){

        for(int i=start;i<=end;i++){

            int sum=0;
            int no=i;
            while(no!=0){

                int rem=no%10;
                int fact=1;

                for(int j=1;j<=rem;j++){

                     fact=fact*j;
                }
                sum=sum+fact;
                no=no/10;
            }
            if(sum==i){
                System.out.print(i+" ");
            }
        }
    }
    public static void main(String[] args) {
        
         Scanner sc=new Scanner(System.in);
        System.out.println("Enter start");
        int start=sc.nextInt();

        System.out.println("Enter end");
        int end=sc.nextInt();

        PrintStrongNo(start,end);

    }
}

