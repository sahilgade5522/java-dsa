import java.util.Scanner;

public class pro4 {

    public static void printOddNo(int start,int end){

        for(int i=start;i<=end;i++){

            if(i%2==1){
                System.out.println(i);
            }
        }
    }
    public static void main(String[] args) {
        
         Scanner sc=new Scanner(System.in);
        System.out.println("Enter start");
        int start=sc.nextInt();

        System.out.println("Enter end");
        int end=sc.nextInt();

        printOddNo(start,end);

    }
}
