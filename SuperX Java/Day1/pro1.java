import java.util.*;
class pro1{

    public static void PrintPattern(int n){

        for(int i=1;i<=n;i++){
            int no=i;

            for(int j=1;j<=n;j++){

                System.out.print(no+" ");
                no++;
            }
            System.out.println();
        }
    }
    public static void main(String[] args) {
        
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter n");
        int n=sc.nextInt();

        PrintPattern(n);

    }
}