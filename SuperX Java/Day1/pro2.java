import java.util.Scanner;
public class pro2 {

    public static void PrintPattern2(int n){
        int no=1;
        for(int i=1;i<=n;i++){

            for(int j=1;j<=i;j++){

                System.out.print(no+" ");
                no++;
            }
            System.out.println();
            no--;
        }
    }
    public static void main(String[] args) {
         Scanner sc=new Scanner(System.in);
        System.out.println("Enter n");
        int n=sc.nextInt();

        PrintPattern2(n);
    }
}
