//In Overidding Access Specifier plays important role if we put bigger Access Specifier in Parent class Method and if we put Weaker AccesSpecifer in child it gives Error So In child Class Acces Specifier if Method  Should Stronger Than Parent class Method.

class Parent{

         void fun(){

                System.out.println("In Parent");
        }

}

class child extends Parent{

    public void fun(){

                 System.out.println("In child");
        }
}

class client{

        public static void main(String args[]){

                Parent obj=new child();
                obj.fun();
        }
}
           
