//if return type is class type then if we write (Object) in Parent class and other classes in child class as return type then it run no error of it. It gives chance to child class to execute its methods.
//
//((--> Covarient Return type ahe))

class Parent{

        Object fun(){

                System.out.println("In Parent");
                return new Object();
	}

}

class child extends Parent{

        String fun(){

                 System.out.println("In child");
		 return "String";
        }
}

class client{

        public static void main(String args[]){

                Parent obj=new child();
                obj.fun();
        }
}                
