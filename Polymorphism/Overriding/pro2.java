
class Parent{

        Parent(){

        System.out.println("Parent constructor");
      }

        void fun(int x){

                System.out.println("Parent fun");
        }

}

class Child extends Parent{

        Child(){

                 System.out.println("Child constructor");
        }

        void fun(int y){

                 System.out.println("Child fun");
        }
}


class client{

        public static void main(String args[]){

                Parent obj=new Child();
                obj.fun(10);
        }
}
         
