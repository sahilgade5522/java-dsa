// If return type is primitive data type then in overridding return type should be same of both methods.

class Parent{

        char fun(){

                System.out.println("In Parent");
		return 'a';
        }

}

class child extends Parent{

        char fun(){

                 System.out.println("In child");
		 return 'a';
        }

}

class client{

        public static void main(String args[]){

                Parent obj=new child();
                obj.fun();
        }
}
                
