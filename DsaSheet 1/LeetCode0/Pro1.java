import java.util.*;
class Pro1{

    public static void main(String[] args) {
        
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter x");
        int x= sc.nextInt();
        int reverse=0;
        while(x!=0){

            int rem=x%10;
            x=x/10;

             reverse=reverse*10+rem;
        }

        System.out.println(reverse);

    }
}